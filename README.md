##Vue JS bubbles  
**Best viewed in Chrome or FF because Safari can't handle the filters**  

ViewJS bubbles is a procedurally generated visual experiment using the ViewJS framework.  
[Preview here.](http://www.cedricindra.com/transfer/code/bubbles/bubbles.html)

**I've noticed something particular with mouse interraction :**  
Even if my repeated instances don't have a mouse listener, all instances pause in contact with the mouse.
Is it because Vue components are constantly listenning for the mouse and triggering events despite not being called by the user ?  

**This is what's happening under the hood :**   
Generate a bunch of instances of dot.js that have a mind of their own.  
The svg filter makes it so they coagulate into blobs.  
~~They jump on mouseenter.~~ *They shrink on mouseleave.*  
Control settings with the menu.   
If the dots take too much space on screen, click reset to restart the process.   

Voila! 

