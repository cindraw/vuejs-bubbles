/* 
This controls the divs in the body of the html
 */

/* 
inner is where all the instances appear
props are generated from outer and pass through here to be used by instances of dot.js */
let inner = new Vue({
	el: '#container',
	data: {
		props: {
			animate: true,
			speed: 5,
			amount: 30,
			mouse: {
				x: undefined,
				y: undefined,
			},
			follow: undefined
		}
	},
	created: () => {
		console.log('loaded', this)
	},
	methods: {
		/* 
		get the mouse x,y and send into instances of dot.js
		used for following the mouse
		*/
		getMouse(e) {
			this.props.mouse.x = e.clientX
			this.props.mouse.y = e.clientY
		}
	}
});

/* menu buttons */
let outer = new Vue({
	el: '#menu',
	data: {
		amount: undefined,
		originalAmount: undefined,
		startstopbutton: 'stop',
		followbutton: 'start',
		speed: 0
	},
	mounted() {
		// set values from inner 
		setTimeout(() => {
			outer.amount = inner.props.amount
			// default value amount for reset function
			outer.originalAmount = inner.props.amount
		}, 0)

	},
	methods: {
		// add remove dots
		amountMore() {
			inner.props.amount += 1
			outer.amount = inner.props.amount
		},
		amountLess() {
			if (inner.props.amount > 0) inner.props.amount -= 1
			outer.amount = inner.props.amount
		},
		// faster slower animation
		speedMore() {
			if (inner.props.speed > 1) inner.props.speed -= 1
			outer.speed = 5 - inner.props.speed
		},
		speedLess() {
			inner.props.speed += 1
			outer.speed = 5 - inner.props.speed
		},
		// start stop animation
		startstop() {
			if (this.startstopbutton === 'stop') {
				this.startstopbutton = 'start'
				inner.props.animate = false
			} else {
				this.startstopbutton = 'stop'
				inner.props.animate = true
			}
		},
		// follow the mouse
		follow() {
			if (this.followbutton === 'start') {
				this.followbutton = 'stop'
				inner.props.follow = true
			} else {
				this.followbutton = 'start'
				inner.props.follow = false
			}
		},
		// reset dots
		reset() {
			inner.props.amount = 0
			setTimeout(() => {
				inner.props.amount = outer.originalAmount
				outer.amount = inner.props.amount
			}, 33)

		}
	},
})