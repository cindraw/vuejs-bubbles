/* 
dots are independent buggers that move and interract on their own
they receive props from the parent component and the menu
they have some mouse interractivity : they jump out of the way on mouse enter or they shrink˝
 */

Vue.component('customcomponent', {

	template: '<div v-bind:style = "mydata.styleobj" class="dots" v-bind:id="\'dot-\'+this._uid" @mouseleave="onEnter($event)"></div>',

	props: ['speed', 'animate', 'mouse', 'follow'],

	data() {
		return {
			mydata: {
				styleobj: {
					cursor: 'default',
					position: 'fixed',
					backgroundColor: 'white',
					transform: 'translate(-50%,-50%)',
					borderRadius: '50%',
					scale: 1,
					opacity: 1,
					width: undefined,
					height: undefined,
					left: undefined,
					top: undefined,
				},
				message: 'component ',
				// number defines init size
				thesize: Math.random() * self.window.innerWidth / 8 + 40,
				delta: {
					size: 20,
					speed: this.speed
				},
				minimum: {
					size: 100,
					blur: 20,
					opacity: 0.2
				},
				interval: undefined,
				_mouse: {
					x: undefined,
					y: undefined
				},
				_follow: false,
				jumping: false,
			}
		}
	},

	mounted() {
		this.init()

		// interval bool so that when blobs follow mouse it doesn't jitter
		let bool = true
		setInterval(() => bool = !bool, 500)

		// watch for input changes affected from the menu  [props] 
		this.$watch('speed', function (n, o) {
			this.mydata.delta.speed = n
			this.changeStyle()
			this.startAnimation(n)
		})

		this.$watch('animate', function (n, o) {
			if (n) this.init()
			else this.stopAnimation()
		})

		this.$watch('mouse.x', function (n, o) {
			this.mydata._mouse.x = n
			if (bool && this.animate) this.init()
		})

		this.$watch('mouse.y', function (n, o) {
			this.mydata._mouse.y = n
		})

		this.$watch('follow', function (n, o) {
			this.mydata._follow = n
		})

	},

	methods: {

		message(message) {
			message.forEach(i => console.log(i))
		},

		init() {
			//init sequence
			this.startAnimation(this.mydata.delta.speed)
			this.changeStyle()
			// //change position so that animation starts immediately instead of waiting for set interval at this.startAnimation()
			setTimeout(() => this.changePosition(), 1)
		},

		startAnimation(speed) {
			clearInterval(this.mydata.interval)
			let time = speed * 1000
			this.mydata.interval = setInterval(() => {
				this.changeStyle()
			}, time)
		},

		stopAnimation() {
			clearInterval(this.mydata.interval)
		},

		// change style is the main controller 
		changeStyle() {
			// change stylobj : changing opacity and blur will messup the svg filter
			this.changeSize()
			if (!this.mydata.jumping) this.changePosition()
			this.changeSpeed()
		},

		changeSpeed() {
			this.mydata.styleobj.transition = `all ${this.mydata.delta.speed}s ease-out`
			this.mydata.styleobj.webkitTransition = `all ${this.mydata.delta.speed}s ease-out`
		},

		changeSize() {
			this.mydata.thesize += (Math.trunc(Math.random() * 3) - 1) * this.mydata.delta.size
			this.mydata.styleobj.width = this.mydata.thesize + 'px'
			this.mydata.styleobj.height = this.mydata.thesize + 'px'
		},

		changePosition() {
			const
				positionX = this.mydata._follow ? this.mydata._mouse.x + Math.trunc((Math.random() * 500)) - 250 : Math.trunc((
					Math.random() * window.innerWidth)),
				positionY = this.mydata._follow ? this.mydata._mouse.y + Math.trunc((Math.random() * 500)) - 250 : Math.trunc((
					Math.random() * window.innerHeight))

			this.mydata.styleobj.left = positionX + 'px'
			this.mydata.styleobj.top = positionY + 'px'
		},

		// mouse interraction
		onEnter(event) {
			// this.jump()
			this.shrink()
		},

		// jump is get out of the way of the mouse (currently unused)
		jump() {
			const
				jumpspeed = this.mydata.delta.speed > 1 ? 0.2 : this.mydata.delta.speed / 10,
				distance = this.mydata.thesize,
				positionX = this.mydata._mouse.x + (Math.trunc((Math.random() * distance)) - distance / 2) * 2,
				positionY = this.mydata._mouse.y + (Math.trunc((Math.random() * distance)) - distance / 2) * 2

			this.mydata.jumping = true

			this.mydata.styleobj.left = positionX + 'px'
			this.mydata.styleobj.top = positionY + 'px'

			this.mydata.styleobj.transition = `all ${jumpspeed}s ease-out`
			this.mydata.styleobj.webkitTransition = `all ${jumpspeed}s ease-out`

			this.changeSize()

			setTimeout(() => {
				this.mydata.jumping = false
			}, jumpspeed);

		},

		// shrink on touch
		shrink() {
			const
				jumpspeed = 0.1
			
			this.mydata.jumping = true
			this.mydata.thesize = (Math.trunc(Math.random() * 3) - 1) * this.mydata.delta.size / 5
			this.mydata.styleobj.width = this.mydata.thesize + 'px'
			this.mydata.styleobj.height = this.mydata.thesize + 'px'

			this.mydata.styleobj.transition = `all ${jumpspeed}s ease-out`
			this.mydata.styleobj.webkitTransition = `all ${jumpspeed}s ease-out`

			setTimeout(() => {
				this.mydata.jumping = false
			}, jumpspeed);
		}

	}
})